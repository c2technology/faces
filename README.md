# Overview

Leading scientists have proven, via science, that learning your coworker�s names
while starting a new job is useful. This application provides a server-side API
for applications to leverage in developing solutions to test retention of 
putting names to faces and vice-versa.

This API is simple in design and is limited to matching names to faces, and 
faces to names. 

# API

## Resources

There are 2 main resources in this API: `face` and `name`.

### Face

This resource describes a unique Face identifier and a URL where an image of the face may be found. A face is guaranteed to belong to a known Name.

| Key | Type   | Description               |
|-----|--------|---------------------------|
| id  | string | The ID of a Face          |
| url | string | The URL of the Face image |

Example:
```
    face: {
        id: "3BdQSQcuMgcs00qYoOuYSY",
        url: "//images.service/2UUFBWewvYsyYIsC08ugWK/5MsUfUPfPOIwgIMaG4WEWq/82057cf4bdba765547db325be5738f6d/image_name.jpg"
    },
```

### Name

This resource describes a unique Name `id`, a `name`. Each Name is guaranteed to be associated with a Face.

| Key   | Type   | Description      |
|-------|--------|------------------|
| id    | string | The ID of a Name |
| name  | string | The full name    |

Example:
```
    {
        id: "5d4QLKn9hCquWgYqu48sE8",
        name: "Jane Doe"
    },
```

## Name the Face API

This collection of services provides an API for a variant of the Face Game where a Face is presented with optional Names from which to select. The intent of this variant is to allow a user to guess the Name of the Face and submit that guess to the Guess Service. To use this variant, clients may start a game by retrieving game information from the Faces Service and may submit guesses to the Face Guess Service.

### Faces Service

This service returns a single face and `n` names. One of the names is guaranteed to be a match to the face provided.

| Action | URI              |
|--------|------------------|
| GET    | /face            |
| POST   | /face/{id}/guess |

#### Setup

This service provides the basic information to start a Name the Face game.

##### Request

 - Method: `GET /face?n=6`
 - Parameters:
    1. n (optional, default=6): the number of names to retrieve

##### Response

The response contains a Face and a collection of Names. One of the Names is guaranteed to be the answer for the given Face.

| Key   | Type   | Description                                                                                                       |
|-------|--------|-------------------------------------------------------------------------------------------------------------------|
| face  | Face   | The face to show as the question in the Name the Face game                                                        |
| names | Name[] | A collection of Names as potential answers for the Face. One of these is guaranteed to be the answer for the Face |

Example:
```
    {
        face: {
            id: "3BdQSQcuMgcs00qYoOuYSY",
            url: "https//images.service/2UUFBWewvYsyYIsC08ugWK/5MsUfUPfPOIwgIMaG4WEWq/82057cf4bdba765547db325be5738f6d/image_name.jpg"
        }, 
        names: [
            {
                id: "5d4QLKn9hCquWgYqu48sE8",
                name: "Jane Doe"
            },{
                id: "20Y46LxW32keOE40WSug0m",
                name: "John Doe"    
            }
        ]
    }
```

###### Response Codes:

| Code | Indication |
|------|------------|
| 200  | OK         |

#### Guess Service

This service allows a client to submit a guess for any given Face `id`. The response indicates wether the guess is correct and the correct answer.

##### Request

 - Method: `POST /face/{id}/guess`
 - Parameters:
    1. `id`: the Face ID for the guess provided in the Faces Service

##### Response

The `id` in the request is of the Name

| Key | Type   | Description              |
|-----|--------|--------------------------|
| id  | string | The Name ID of the guess |

Example: 
```
    {
        id: "5d4QLKn9hCquWgYqu48sE8"
    }
```

#### Response

The response contains the result of the guess and the correct Name as the answer.

| Key    | Type    | Description                                                                           |
|--------|---------|---------------------------------------------------------------------------------------|
| result | boolean | The result of the guess. True indicates a successful guess, false indicates otherwise |
| answer | Face    | The true answer for the original question.                                            |

Example:
```
    {
        result: true,
        answer: {
            id: "5d4QLKn9hCquWgYqu48sE8",
            name: "Jane Doe"
        }
    }
```

###### Response Codes:

| Code | Indication |
|------|------------|
| 200  | OK         |

## Face the Name API

This collection of services provides an API for a variant of the Face Game where a Name is presented with optional Faces from which to select. The intent of this variant is to allow a user to guess the Face of a Name and submit that guess to the Guess Service. To use this variant, clients may start a game by retrieving game information from the Name Service and may submit guesses to the Name Guess Service.

### Name Service

This service returns a single Name and `n` Faces. One of the Faces is guaranteed to be a match to the Name provided.

| Action | URI              |
|--------|------------------|
| GET    | /name            |
| POST   | /name/{id}/guess |

#### Setup

This service provides the basic information to start a Face the Name game.

##### Request

 - Method: `GET /name?n=6`
 - Parameters:
    1. n (optional, default=6): the number of Faces to retrieve

##### Response

The response contains a Name and a collection of Faces. One of the Faces is guaranteed to be the answer for the given Name.

| Key   | Type   | Description                                                                                         |
|-------|--------|-----------------------------------------------------------------------------------------------------|
| name  | Name   | The Name to show as the question in the Face the Name game                                          |
| faces | Face[] | A collection of Faces as potential answers to the Name. One of these is guaranteed to be the answer |

Example:
```
    {
        name: {
            id: "5d4QLKn9hCquWgYqu48sE8",
            name: "Jane Doe"
        }, 
        faces: [{
            id: "3BdQSQcuMgcs00qYoOuYSY",
            image: "//images.service/2UUFBWewvYsyYIsC08ugWK/5MsUfUPfPOIwgIMaG4WEWq/82057cf4bdba765547db325be5738f6d/image_name.jpg"
        },{
            id: "5cEKDmCEreguEQoI0UyyIU",
            image: "//images.service/2Ee0kv4vAMGW6ukaWIQsiq/5AtfuGiuvmUuEWeI00QwAa/3cttzl4i3k1h/image_name.jpg"
        }]        
    }
```

###### Response Codes:

| Code | Indication |
|------|------------|
| 200  | OK         |

#### Guess Service

This service allows a client to submit a guess for any given Name `id`. The response indicates wether the guess is correct and the correct answer.

##### Request

 - Method: `POST /name/{id}/guess`
 - Parameters:
    1. `id`: the Name ID for the guess provided

##### Response

The `id` in the request is of the Face

| Key | Type   | Description              |
|-----|--------|--------------------------|
| id  | string | The Face ID of the guess |

Example: 
```
    {
        id: "3BdQSQcuMgcs00qYoOuYSY"
    }
```

#### Response

The response contains the result of the guess and the correct Face as the answer.

| Key    | Type    | Description                                                                           |
|--------|---------|---------------------------------------------------------------------------------------|
| result | boolean | The result of the guess. True indicates a successful guess, false indicates otherwise |
| answer | Name    | The true answer for the original question.                                            |

Example:
```
    {
        result: true,
        answer: {
            id: "5d4QLKn9hCquWgYqu48sE8",
            name: "Jane Doe"
        }
    }
```

###### Response Codes:

| Code | Indication |
|------|------------|
| 200  | OK         |
 
## Future Features

[ ] Overall Stats service 
[ ] Stats by face
[ ] Stats by name
[ ] Least identifiable name
[ ] Least identifiable face
[ ] Most identifiable name
[ ] Most identifiable face
[ ] Limit game scope to partially matched names

# Building from Source

This project uses Maven for dependency management and compliation. To build, you
must run:

`mvn clean install`

This will build a WAR file with dependencies included. To deploy this, you may 
deploy it to an existing Tomcat server, or you can run it in a Docker container.
To run it in a Docker container, you must have both Docker and Docker Compose
installed. To install using Docker Compose run:

`docker-compose up`

Once the deployment completes, you may access this project at: 
http://localhost:8080/faces/v1/name