/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data.willowtree;

import java.io.IOException;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.data.Cache;
import net.c2technology.faces.api.DataSource;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.data.Name;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chris Ryan
 */
public class WillowTreeDataSourceTest {

    public WillowTreeDataSourceTest() {
    }

    private DataSource instance;

    @Before
    public void setUp() {
        try {
            instance = new WillowTreeDataSource("https://www.willowtreeapps.com/api/v1.0/profiles");
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of update method, of class WillowTreeDataSource.
     */
    @Test
    public void testUpdate() throws Exception {
        System.out.println("update");
        Name expName = new Name("4NCJTL13UkK0qEIAAcg4IQ", "Joel Garrett");
        Face expFace = new Face("4Mv2CONANym46UwuuCIgK", "//images.ctfassets.net/3cttzl4i3k1h/4Mv2CONANym46UwuuCIgK/cbeb43c93a843a43c07b1de9954795e2/headshot_joel_garrett.jpg");
        Data cache = new Cache();
        instance.update(cache);
        
        assertEquals(expName, cache.getName("4NCJTL13UkK0qEIAAcg4IQ"));
        assertEquals(expName, cache.getName("4Mv2CONANym46UwuuCIgK"));
        assertEquals(expFace, cache.getFace("4NCJTL13UkK0qEIAAcg4IQ"));
        assertEquals(expFace, cache.getFace("4Mv2CONANym46UwuuCIgK"));
    }

}
