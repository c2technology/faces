/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data;

import java.util.Collection;
import java.util.Random;
import net.c2technology.faces.api.Data;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chris Ryan
 */
public class CacheTest {

    public CacheTest() {
    }

    /**
     * Test of update method, of class Cache.
     */
    @Test
    public void testUpdate() {
        System.out.println("update");
        Name name = new Name("name id", "Full Name");
        Face face = new Face("face id", "base64image");
        Cache instance = new Cache();
        instance.add(name, face);
        assertEquals(face, instance.getFace(face.getID()));
        assertEquals(face, instance.getFace(name.getID()));

        assertEquals(name, instance.getName(name.getID()));
        assertEquals(name, instance.getName(face.getID()));

    }

    /**
     * Test of getNames method, of class Cache.
     */
    @Test
    public void testGetNames_limit0() {
        System.out.println("getNames limit 0");
        int limit = 0;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Name> result = instance.getNames(limit);
        assertEquals(limit, result.size());
    }

    @Test
    public void testGetNames_limit1() {
        System.out.println("getNames limit 1");
        int limit = 1;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Name> result = instance.getNames(limit);
        assertEquals(limit, result.size());
    }

    @Test
    public void testGetNames_limitless1() {
        System.out.println("getNames limit -1");
        int limit = -1;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Name> result = instance.getNames(limit);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetNames_limit100() {
        System.out.println("getNames limit 100");
        int limit = 100;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Name> result = instance.getNames(limit);
        assertEquals(3, result.size());
    }

    /**
     * Test of getFace method, of class Cache.
     */
    @Test
    public void testGetFace() {
        System.out.println("getFace");
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        assertEquals(face1, instance.getFace("face 1"));
        assertEquals(face1, instance.getFace("name 1"));
    }

    /**
     * Test of getFaces method, of class Cache.
     */
    @Test
    public void testGetFaces_limit0() {
        System.out.println("getFaces limit 0");
        int limit = 0;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Face> result = instance.getFaces(limit);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetFaces_limitless0() {
        System.out.println("getFaces limit -1");
        int limit = -1;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Face> result = instance.getFaces(limit);
        assertEquals(0, result.size());
    }

    @Test
    public void testGetFaces_limit1() {
        System.out.println("getFaces limit 1");
        int limit = 1;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);

        Collection<Face> result = instance.getFaces(limit);
        assertEquals(1, result.size());
    }

    @Test
    public void testGetFaces_limit100() {
        System.out.println("getFaces limit 100");
        int limit = 100;
        Name name1 = new Name("name 1", "Full Name");
        Name name2 = new Name("name 2", "Full Name");
        Name name3 = new Name("name 3", "Full Name");
        Face face1 = new Face("face 1", "image1");
        Face face2 = new Face("face 2", "image2");
        Face face3 = new Face("face 3", "image3");
        Data instance = new Cache();
        instance.add(name1, face1);
        instance.add(name2, face2);
        instance.add(name3, face3);
        Collection<Face> result = instance.getFaces(limit);
        assertEquals(3, result.size());
    }

    /**
     * Randomized test
     */
    @Test
    public void testGetFaces_limitN() {
        System.out.println("getFaces limit N");
        int runs = 100;
        int ceiling = 100;
        for (int r = 0; r < runs; r++) {
            Data instance = new Cache();
            //populate a random amount of data
            int size = new Random().nextInt(ceiling);
            for (int i = 0; i < size; i++) {
                Name n = new Name("name " + i, "Name " + i);
                Face f = new Face("face " + i, "Image " + i);
                instance.add(n, f);
            }
            //retrieve a random number of data
            int limit = new Random().nextInt(ceiling);
            Collection<Face> result = instance.getFaces(limit);
            assertEquals(Math.min(limit, size), result.size());
        }
    }
    
    /**
     * Randomized test
     */
    @Test
    public void testGetNames_limitN() {
        System.out.println("getNames limit N");
        int runs = 100;
        int ceiling = 100;
        for (int r = 0; r < runs; r++) {
            Data instance = new Cache();
            //populate a random amount of data
            int size = new Random().nextInt(ceiling);
            for (int i = 0; i < size; i++) {
                Name n = new Name("name " + i, "Name " + i);
                Face f = new Face("face " + i, "Image " + i);
                instance.add(n, f);
            }
            //retrieve a random number of data
            int limit = new Random().nextInt(ceiling);
            Collection<Name> result = instance.getNames(limit);
            assertEquals(Math.min(limit, size), result.size());
        }
    }
}
