/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Chris Ryan
 */
public class NameTest {

    public NameTest() {
    }

    private static final String ID = "id";
    private static final String NAME = "full name";
    private Name instance;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        instance = new Name(ID, NAME);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getId method, of class Name.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        assertEquals(ID, instance.getID());
    }

    /**
     * Test of getName method, of class Name.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        assertEquals(NAME, instance.getName());
    }

    /**
     * Test of hashCode method, of class Name.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -1735208762;
        assertEquals(expResult, instance.hashCode());
    }

    /**
     * Test of equals method, of class Name.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Name otherInstance = new Name(ID, NAME);
        assertTrue(instance.equals(otherInstance));
    }

    @Test
    public void testEquals_inequal() {
        System.out.println("equals - false");
        Name otherInstance = new Name("ID", "NAME");
        assertFalse(instance.equals(otherInstance));
    }

}
