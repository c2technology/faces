/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Chris Ryan
 */
public class FaceTest {

    public FaceTest() {
    }

    private static final String ID = "id";
    private static final String IMAGE = "base64 encoded image";
    private Face instance;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        instance = new Face(ID, IMAGE);
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getId method, of class Face.
     */
    @Test
    public void testGetID() {
        System.out.println("getID");
        assertEquals(ID, instance.getID());
    }

    /**
     * Test of getImage method, of class Face.
     */
    @Test
    public void testGetImage() {
        System.out.println("getImage");
        assertEquals(IMAGE, instance.getImage());
    }

    /**
     * Test of hashCode method, of class Face.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        int expResult = -2138533974;
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Face.
     */
    @Test
    public void testEquals_equal() {
        System.out.println("equals");
        Face otherInstance = new Face(ID, IMAGE);
        assertTrue(instance.equals(otherInstance));
    }

    @Test
    public void testEquals_inequal() {
        System.out.println("equals - false");
        Face otherInstance = new Face("ID", "IMAGE");
        assertFalse(instance.equals(otherInstance));
    }

}
