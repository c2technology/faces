/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces;

import net.c2technology.faces.api.Data;
import net.c2technology.faces.service.Injector;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chris Ryan
 */
public class InitializationTest {

    public InitializationTest() {
    }

    /**
     * Test of contextInitialized method, of class Initialization.
     */
    @Test
    public void testContextInitialized() {
        System.out.println("contextInitialized");
        Initialization instance = new Initialization();
        instance.contextInitialized(null);
        assertTrue(Injector.hasBinding(Data.class));
    }

    /**
     * Test of contextDestroyed method, of class Initialization.
     */
    @Test
    public void testContextDestroyed() {
        System.out.println("contextDestroyed");
        Initialization instance = new Initialization();
        instance.contextInitialized(null);
        assertTrue(Injector.hasBinding(Data.class));
        instance.contextDestroyed(null);
        assertFalse(Injector.hasBinding(Data.class));
    }

}
