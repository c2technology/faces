/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.api;

import java.util.Collection;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.data.Name;

/**
 * Handles persistence for {@link Person} data.
 *
 * @author Chris
 */
public interface Data {

    /**
     * Retrieves a specific {@link Name} by its ID or it's {@link Face} ID.
     *
     * @param id of the {@link Name} or the {@link Face} for the {@code Name} to
     * lookup.
     * @return all stored {@code Name} values
     */
    public Name getName(String id);

    /**
     * Retrieves a number of random {@link Name} records limited by
     * {@code limit}. If {@code limit} is less than 1, all {@link Name} records
     * are returned
     *
     * @param limit results when value is greater than 0
     * @return all {@link Name} records limited by {@code limit}
     */
    public Collection<Name> getNames(int limit);

    /**
     * Retrieves a specific {@link Face} by its ID or the ID of it's associated
     * {@link Name}.
     *
     * @param id of the {@link Face} to lookup by its ID or its {@link Name} ID.
     * @return all stored {@code Face} values
     */
    public Face getFace(String id);

    /**
     * Retrieves a number of random {@link Face} records limited by
     * {@code limit}. If {@code limit} is less than 1, all {@link Face} records
     * are returned
     *
     * @param limit results when value is greater than 0
     * @return all {@link Face} records limited by {@code limit}
     */
    public Collection<Face> getFaces(int limit);

    /**
     * Adds a {@link Name} and a {@link Face} associating the two together
     *
     * @param name
     * @param face
     */
    public void add(Name name, Face face);

    //TODO: Stats
//    public void addCorrectFace(String name);
//
//    public void addWrongFace(String name);
//
//    public void addCorrectName(String name);
//
//    public void addWrongName(String name);
//
//    public Set<FaceStat> getFaceStats();
//
//    public Set<NameStat> getNameStats();
}
