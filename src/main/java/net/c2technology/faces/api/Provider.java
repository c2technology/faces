/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.api;

/**
 * API for ingesting third-party data. An implementing {@code Provider} should
 * handle configurations and setup on the "how" for ingesting data. The ingested
 * data is provided to consumers via the {@link #read()} method in the format
 * {@code T} identified in the implementing class.
 *
 * @author Chris Ryan
 * @param <T> The type of object translated by the implementing {@code Provider}
 */
public interface Provider<T> {

    /**
     * Reads and translates arbitrary data to {@code T}.
     *
     * @return
     * @throws Exception
     */
    public T read() throws Exception;
}
