/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.service;

import net.c2technology.faces.resource.Answer;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.data.Name;
import net.c2technology.faces.resource.Question;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The service for the "name the face" game. This service should be used when
 * the game presents a single name and the user must provide a face as the
 * answer.
 *
 * @author Chris
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("/name")
public class NameService {

    private static final Logger LOG = LogManager.getLogger(NameService.class);
    private final Data data = Injector.inject(Data.class);

    /**
     * Retrieves data for a new "face the name" game. The data returned is
     * dependent on the given query parameters:
     * <ul>
     * <li>{@code n} (optional): identifies the maximum number of faces to
     * return. 0 is a valid option, in which case no faces are returned.</li>
     * </ul>
     *
     * Any combination of query parameters may be used. If a combination of
     * parameters result in 0 results, an empty response is returned.
     *
     * @param limit the number of names in the response
     * @return a {@link FaceResource} with a {@code limit} of faces . If
     * @throws java.lang.Exception if this service fails to return a valid
     * response
     */
    @GET
    public Question<Name, Face> read(@QueryParam("n") @DefaultValue("6") int limit) throws Exception {
        //check to see if we need to do any work
        LOG.info("Getting names limited to " + limit);
        if (limit < 1) {
            return new Question();
        }

        Name question = data.getNames(1).iterator().next();
        Face answer = data.getFace(question.getID());
        Set<Face> options = new HashSet();
        options.add(answer);
        while (options.size() < limit) {
            options.addAll(data.getFaces(1));
        }
        return new Question(question, options);
    }

    /**
     * Determines if the given answer in the POST body is the correct answer and
     * returns the correct answer in either case.
     *
     * @param guessID
     * @param questionID
     * @return
     * @throws Exception
     */
    @POST
    @Path("/{id}/guess")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Answer<Face> create(@PathParam("id") String questionID, @FormParam("id") String guessID) throws Exception {
        Face answer = data.getFace(questionID);
        boolean result = answer.getID().equals(guessID);
        //TODO: Stats
//        if (result) {
//            data.addCorrectFace(guess.getName());
//        }
        return new Answer<Face>(result, answer);
    }
}
