/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.service;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A generic factory for dependency injection.
 *
 * @author Chris
 */
public class Injector {

    private final Map<Class, Object> registrar = new HashMap();
    private static final Injector INSTANCE = new Injector();
    private static final Logger LOG = LogManager.getLogger(Injector.class);

    private Injector() {
    }

    public <T> T getProvider(Class<T> clazz) {
        if (registrar.containsKey(clazz)) {
            return clazz.cast(registrar.get(clazz));
        }
        throw new IllegalArgumentException(String.format("Provider of type %s has not been registered!", clazz.getName()));
    }

    public void registerProvider(Class clazz, Object provider) {
        if (clazz.isInstance(provider)) {
            registrar.put(clazz, provider);
        } else {
            throw new IllegalArgumentException(String.format("Provided %s is not a subclass of %s", provider.getClass().getName(), clazz.getName()));
        }
    }

    /**
     * Binds the given {@code clazz} to the {@code provider} implementation. The
     * {@code provider} must be of type {@code clazz} or a sub-type. When
     * injecting, the injected implementation should be cast to the
     * {@code clazz} given when binding.
     *
     * @param clazz
     * @param provider
     */
    public static void bind(Class clazz, Object provider) {
        LOG.debug("Binding {} to {}", provider.getClass().getName(), clazz.getCanonicalName());
        INSTANCE.registerProvider(clazz, provider);
    }

    /**
     * Destroys bindings. All injection references are lost. Allows for GC.
     */
    public static void destroy() {
        LOG.debug("Destroying binder...");
        INSTANCE.registrar.clear();
    }

    /**
     * Used when injecting an implementation of the given {@code clazz}, if no
     * binding for {@code clazz} is found, an {@code IllegalArgumentException}
     * is thrown. To prevent this behavior, first check if a binding exists by
     * using {@link #hasBinding(java.lang.Class)}.
     *
     * @param clazz
     * @return
     */
    public static <T> T inject(Class<T> clazz) throws IllegalArgumentException {
        LOG.trace("Injecting {}...", clazz.getCanonicalName());
        return INSTANCE.getProvider(clazz);
    }

    /**
     * Determines if a binding exists for the given {@code clazz}. If no binding
     * exists, {@code false} is returned. This check should be done before
     * calling {@link #inject(java.lang.Class)} to determine if a binding
     * exists.
     *
     * @param clazz
     * @return
     */
    public static boolean hasBinding(Class clazz) {
        return (INSTANCE.registrar.containsKey(clazz));
    }

}
