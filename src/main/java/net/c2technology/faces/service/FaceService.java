/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.c2technology.faces.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.data.Name;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.resource.Answer;
import net.c2technology.faces.resource.Question;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The service for the "name the face" game. This service should be used when
 * the game presents a single face and the user must provide a name as the
 * answer.
 *
 * @author Chris Ryan
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("/face")
public class FaceService {

    private static final Logger LOG = LogManager.getLogger(FaceService.class);

    private final Data data = Injector.inject(Data.class);

    /**
     * Retrieves data for a new name the face game. The data returned is
     * dependent on the given query parameters:
     * <ul>
     * <li>{@code n} (optional): identifies the maximum number of names to
     * return. 0 is a valid option, in which case no names are returned.</li>
     * </ul>
     *
     * Any combination of query parameters may be used. If a combination of
     * parameters result in 0 results, an empty response is returned.
     *
     * @param limit the number of names in the response
     * @return a {@link FaceResource} with a {@code limit} of faces . If
     * @throws java.lang.Exception if this service fails to return a valid
     * response
     */
    @GET
    public Question<Face, Name> setup(@QueryParam(value = "n")
            @DefaultValue(value = "6") int limit) throws Exception {
        //check to see if we need to do any work
        if (limit < 1) {
            return new Question();
        }
        Face question = data.getFaces(1).iterator().next();
        Name answer = data.getName(question.getID());
        Set<Name> options = new HashSet();
        options.add(answer);
        while (options.size() < limit) {
            options.addAll(data.getNames(1));
        }
        return new Question(question, options);
    }

    /**
     * Determines if the given answer in the POST body is the correct answer and
     * returns the correct answer in either case. When providing an answer, the
     * name could be entered manually or selected from a list. In either case,
     * at least the name ID or both the first and last name should be present in
     * the POST body. If this condition is not true, the result will always be
     * {@code false}
     *
     * @param questionID
     * @param guessID
     * @return
     * @throws Exception
     */
    @POST
    @Path("/{id}/guess")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Answer<Name> create(@PathParam("id") String questionID, @FormParam("id") String guessID) throws Exception {
        Name answer = data.getName(questionID);
        boolean result = answer.getID().equals(guessID);
        //TODO: stats
//        if (result) {
//            data.addCorrectName(answer.getName());
//        }
        return new Answer(result, answer);
    }
}
