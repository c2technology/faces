/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.c2technology.faces;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.data.Cache;
import net.c2technology.faces.api.DataSource;
import net.c2technology.faces.data.willowtree.WillowTreeDataSource;
import net.c2technology.faces.service.Injector;

/**
 * An all-in-one {@link ServletContextListener}.
 *
 * @author Chris
 */
public class Initialization implements ServletContextListener {

    private static final Logger LOG = LogManager.getLogger(Initialization.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LOG.info("Starting Faces!");
        try {
            //TODO: move URL to configuration
            Data cache = new Cache();
            DataSource ds = new WillowTreeDataSource("https://www.willowtreeapps.com/api/v1.0/profiles");
            ds.update(cache);
            //TODO: Bind using class loader
            Injector.bind(Data.class, cache);
        } catch (Exception e) {
            //TODO: better exception handling
            LOG.fatal(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Injector.destroy();
    }
}
