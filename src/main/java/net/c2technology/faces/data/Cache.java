/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.api.HasID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * A Data implementation that is backed by a Willow Tree service holding cached
 * data.
 *
 * @author Chris Ryan
 */
public class Cache implements Data {

    private static final Logger LOG = LogManager.getLogger(Cache.class);
    private final Map<String, Name> names;
    private final Map<String, Face> namedFaces;
    private final Map<String, Face> faces;
    private final Map<String, Name> facedNames;

    public Cache() {
        this.names = new HashMap();
        this.faces = new HashMap();
        this.namedFaces = new HashMap();
        this.facedNames = new HashMap();
    }

    @Override
    public void add(Name name, Face face) {
        this.names.put(name.getID(), name);
        this.namedFaces.put(name.getID(), face);

        this.faces.put(face.getID(), face);
        this.facedNames.put(face.getID(), name);
    }

    @Override
    public Name getName(String id) {
        if (this.names.containsKey(id)) {
            return this.names.get(id);
        }
        return this.facedNames.get(id);
    }

    @Override
    public Collection<Name> getNames(int limit) {
        return get(this.names, limit);
    }

    @Override
    public Face getFace(String id) {
        if (this.faces.containsKey(id)) {
            return this.faces.get(id);
        }
        return this.namedFaces.get(id);
    }

    @Override
    public Collection<Face> getFaces(int limit) {
        return get(this.faces, limit);
    }

    /**
     * This gets a generic set from a generic cache limited to {@code limit}
     * entities.
     *
     * @param <T> The generic type contained within the returned Set
     * @param cache The generically typed Map of cached data to use
     * @param limit The most results returned
     * @return a limited subset of the generic cache
     */
    private <T extends HasID> Set<T> get(Map<String, T> cache, int limit) {
        if (limit < 1) {
            return new HashSet();
        }
        int realLimit = Math.min(limit, cache.size());
        Set<T> selected = new HashSet(realLimit);
        List<String> keys = new ArrayList(cache.size());
        keys.addAll(cache.keySet());
        while (selected.size() < realLimit) {
            //TODO: Optimize this to not pick keys that already have been picked
            LOG.info("collected " + selected.size());
            LOG.info("limit " + realLimit);
            LOG.info("remaining keys " + keys.size());
            int random = new Random().nextInt(keys.size());
            String key = keys.remove(random);
            T item = cache.get(key);
            LOG.info("selected item" + item.getID());
            if (selected.contains(item)) {
                LOG.error("Duplicate item: " + item.getID());
            }
            selected.add(item);

        }
        return selected;
    }

}
