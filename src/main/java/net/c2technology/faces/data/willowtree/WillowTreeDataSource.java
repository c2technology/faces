/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.data.willowtree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import net.c2technology.faces.api.Data;
import net.c2technology.faces.api.DataSource;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.data.Name;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A Data implementation that is backed by a Willow Tree service holding cached
 * data.
 *
 * @author Chris Ryan
 */
public class WillowTreeDataSource implements DataSource {

    private static final Logger LOG = LogManager.getLogger(WillowTreeDataSource.class);
    private final String url;

    public WillowTreeDataSource(String url) throws MalformedURLException, IOException {
        this.url = url;
        //TODO: use a translator interface for handling the URL to JSONArray logic so we can mock it in tests
    }

    private JSONArray read() throws MalformedURLException, IOException {
        try (InputStream is = new URL(this.url).openStream()) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(reader);
            return new JSONArray(jsonText);
        }
    }

    @Override
    public void update(Data data) throws Exception {
        JSONArray jsonArray = read();
        for (Object o : jsonArray) {
            //JSONArray has an underlying Iterator
            if (o instanceof JSONObject) {
                JSONObject json = (JSONObject) o;
                try {
                    if (!json.has("id") || !json.has("headshot")) {
                        continue;
                    }
                    JSONObject headshot = json.getJSONObject("headshot");
                    if (!headshot.has("id") || !headshot.has("url")) {
                        continue;
                    }
                    String faceID = headshot.getString("id");
                    String src = headshot.getString("url");
                    String nameID = json.getString("id");
                    String fullName = String.format("%s %s", json.getString("firstName"), json.getString("lastName"));
                    Name name = new Name(nameID, fullName);
                    Face face = new Face(faceID, src);
                    data.add(name, face);
                } catch (JSONException e) {
                    LOG.warn("failed to parse record " + json.toString(), e);
                }
            }
        }
    }

    /**
     * Read all data in the given {@code reader} and return the {@code String}
     *
     * @param reader to read
     * @return read string
     * @throws IOException if reading fails
     */
    private String readAll(Reader reader) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = reader.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}
