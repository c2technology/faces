/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.resource;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * A REST resource to {@link Answer} a {@link Question}
 *
 * @author Chris Ryan
 * @see Question
 * @param <T>
 */
@XmlRootElement
public class Answer<T> {

    private boolean result;
    private T answer;

    public Answer() {
    }

    public Answer(boolean result, T answer) {
        this.result = result;
        this.answer = answer;
    }

    public boolean isResult() {
        return result;
    }

    public T getAnswer() {
        return answer;
    }

}
