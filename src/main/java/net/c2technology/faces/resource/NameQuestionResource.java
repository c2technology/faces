/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.resource;

import java.util.Collection;
import java.util.HashSet;
import javax.xml.bind.annotation.XmlRootElement;
import net.c2technology.faces.data.Face;
import net.c2technology.faces.data.Name;

/**
 * A REST {@link Resource} that represents a {@link Name}
 *
 * @author Chris Ryan
 */
@XmlRootElement
public class NameQuestionResource {

    private Collection<Face> faces;
    private Name name;

    /**
     * Creates an empty {@link Name} {@link Resource}. This is required as part
     * of the JAX RS framework.
     */
    public NameQuestionResource() {
    }

    /**
     * Creates a {@code NameQuestionResource} where the {@link Name} is the
     * question and the available answers are a collection of {@link Face}
     * objects.
     *
     * @param name
     * @param faces
     */
    public NameQuestionResource(Name name, Collection<Face> faces) {
        this.name = name;
        this.faces = new HashSet();
        for (Face f : faces) {
            this.faces.add(f);
        }
    }

    public Collection<Face> getFaces() {
        return faces;
    }

    public Name getName() {
        return name;
    }

}
