/*
 * Copyright (C) 2019 Chris Ryan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package net.c2technology.faces.resource;

import java.util.Collection;
import java.util.HashSet;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * A REST resource that represents a {@link Question} with options
 *
 * @author Chris Ryan
 */
@XmlRootElement
public class Question<Q, O> {

    private Q question;
    private Collection<O> options;

    /**
     * Creates an empty resource. This is required as part of the JAX RS
     * framework.
     */
    public Question() {
    }

    /**
     * Creates a {@code QuestionResource} where the question is an arbitrary
     * data object and the available options are arbitrary data objects.
     *
     * @param question
     * @param options
     */
    public Question(Q question, Collection<O> options) {
        this.question = question;
        this.options = new HashSet();
        for (O o : options) {
            this.options.add(o);
        }
    }

    public Q getQuestion() {
        return question;
    }

    public Collection<O> getOptions() {
        return options;
    }

}
