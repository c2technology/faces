FROM tomcat:7-jdk8
WORKDIR /usr/local/tomcat
COPY target/*.war ${CATALINA_HOME}/webapps/faces.war
EXPOSE 8080
CMD ["catalina.sh", "run"]